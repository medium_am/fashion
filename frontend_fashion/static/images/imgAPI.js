const imgAPI = {
  avatar: [
    '/images/avatars/pp_girl.svg',
    '/images/avatars/pp_girl2.svg',
    '/images/avatars/pp_girl3.svg',
    '/images/avatars/pp_girl4.svg',
    '/images/avatars/pp_girl5.svg',
    '/images/avatars/pp_girl.svg',
    '/images/avatars/pp_boy.svg',
    '/images/avatars/pp_boy2.svg',
    '/images/avatars/pp_boy3.svg',
    '/images/avatars/pp_boy4.svg',
    '/images/avatars/pp_boy5.svg'
  ],
  photo: [
    'https://via.placeholder.com/675x900/e1ad92/fff',
    'https://via.placeholder.com/967x725/ea6d6d/fff',
    'https://via.placeholder.com/1280x849/ea6db7/fff',
    'https://via.placeholder.com/967x725/bb6dea/fff',
    'https://via.placeholder.com/1048x701/6d6fea/fff',
    'https://via.placeholder.com/1050x700/6dc0ea/fff',
    'https://via.placeholder.com/970x725/6deaa6/fff',
    'https://via.placeholder.com/1051x700/b8de27/fff',
    'https://via.placeholder.com/1051x701/de9f27/fff',
    'https://via.placeholder.com/1050x700/ef4545/fff',
    'https://via.placeholder.com/1050x700/ffc4c4/757575',
    'https://via.placeholder.com/640x447/fdffc4/757575',
    'https://via.placeholder.com/1280x851/c4ffd7/757575',
    'https://via.placeholder.com/640x425/c4cdff/757575'
  ],
  fashion: [
    'https://via.placeholder.com/1280x854/000000/FFFFFF',
    'http://localhost/media/promo/promo1.jpg',
    'http://localhost/media/promo/promo2.jpg',
    'http://localhost/media/promo/promo3.jpg',
    'http://localhost/media/promo/promo4.jpg',
    'http://localhost/media/promo/promo1.jpg',
    'https://via.placeholder.com/300x372/fdffa4/757575',
    'http://localhost/media/promo/autumn_promo.jpg',
    'http://localhost/media/promo/autumn_promo.jpg',
    'http://localhost/media/promo/new_trends_2020.jpg',
    'http://localhost/media/promo/men_collection_promo.jpg',
    '/images/fashion/shoes.png',
    '/images/fashion/shoes.png',
    'http://localhost/media/promo/men_style.png',
    'http://localhost/media/promo/men_watch.png',
    '/images/fashion/shoes.png',
    'http://localhost/media/promo/woman_sport.png',
    'http://localhost/media/promo/men_watch.png',
    'http://localhost/media/promo/men_watch.png',
    '/images/fashion/bag.png',
    'http://localhost/media/promo/promo1.jpg',
    '/images/fashion/bag.png',
    '/images/fashion/bag.png',
    '/images/fashion/bag.png',
    '/images/fashion/bag.png',
    '/images/fashion/bag.png',
    'http://localhost/media/promo/testimonals_1.jpg',
    'http://localhost/media/promo/testimonals_2.jpg',
    'http://localhost/media/promo/testimonals_3.jpg',
    'http://localhost/media/promo/testimonals_4.jpg',
    'http://localhost/media/promo/testimonals_5.jpg',
    'http://localhost/media/promo/testimonals_6.jpg',
    'http://localhost/media/promo/testimonals_7.jpg',
    'http://localhost/media/promo/testimonals_8.jpg',
    'http://localhost/media/promo/testimonals_6.jpg',
    'https://via.placeholder.com/516x462/b4c532/757575'
  ]
}

export default imgAPI
