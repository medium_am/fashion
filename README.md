# Fashion clothing store

## Project structure

The project consists of a backend and a frontend part. This localhosted application.

## Preferences and Environment

### Frontend

Install *npm*. After do this command:
```
npm install
```

### Backend

Install *python3*. Create environment:
```
python3 -m venv venv
```

Install requirements.
```
. venv/bin/activate
pip install -U pip
pip install -r req.txt
```

### Getting started
1. Run frontend `npm run dev`
2. Run backend `venv/bin/python manage.py runserver`
