from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import Shoes, Bag
from .serializers import ShoesSerializer, BagSerializer
from django_filters.rest_framework import DjangoFilterBackend

# Create your views here.


class ShoesView(ListCreateAPIView):
    queryset = Shoes.objects.all()
    serializer_class = ShoesSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('gender', 'item_code',)


class SingleShoesView(RetrieveUpdateDestroyAPIView):
    queryset = Shoes.objects.all()
    serializer_class = ShoesSerializer


class BagView(ListCreateAPIView):
    queryset = Bag.objects.all()
    serializer_class = ShoesSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('gender', 'item_code',)


class SingleBagView(RetrieveUpdateDestroyAPIView):
    queryset = Bag.objects.all()
    serializer_class = ShoesSerializer
