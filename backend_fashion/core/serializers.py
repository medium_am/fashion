from rest_framework import serializers
from .models import Shoes, Bag


class ShoesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shoes
        fields = "__all__"


class BagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bag
        fields = "__all__"
