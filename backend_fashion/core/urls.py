from django.conf.urls import url, include
from .views import ShoesView, SingleShoesView, BagView, SingleBagView

urlpatterns = [
    url(r'^shoes/$', ShoesView.as_view()),
    url(r'^shoes/(?P<pk>\d+)/$', SingleShoesView.as_view()),
    url(r'^bags/$', BagView.as_view()),
    url(r'^bags/(?P<pk>\d+)/$', SingleBagView.as_view()),
]
