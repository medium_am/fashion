from django.contrib import admin
from .models import Shoes, Bag

# Register your models here.


class ShoesAdmin(admin.ModelAdmin):
    list_display = ('id', 'item_code', 'gender', 'price')
    list_filter = ('gender',)
    search_fields = ('id', 'item_code', 'price')


class BagAdmin(admin.ModelAdmin):
    list_display = ('id', 'item_code', 'gender', 'price')
    list_filter = ('gender',)
    search_fields = ('id', 'item_code', 'price')


admin.site.register(Shoes, ShoesAdmin)
admin.site.register(Bag, BagAdmin)
