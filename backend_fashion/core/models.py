from django.db import models

# Create your models here.


class Shoes(models.Model):
    id = models.AutoField(primary_key=True)
    item_code = models.CharField(blank=False, null=False, max_length=50, unique=True)
    file = models.FileField(blank=False, null=False, upload_to="shoes/%Y/%m/%d/")
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    price = models.PositiveIntegerField(blank=False, null=False)
    describe = models.CharField(max_length=250)

    def __str__(self):
        return self.item_code

    class Meta:
        verbose_name_plural = "shoes"


class Bag(models.Model):
    id = models.AutoField(primary_key=True)
    item_code = models.CharField(blank=False, null=False, max_length=50, unique=True)
    file = models.FileField(blank=False, null=False, upload_to="bags/%Y/%m/%d/")
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    price = models.PositiveIntegerField(blank=False, null=False)
    describe = models.CharField(max_length=250)

    def __str__(self):
        return self.item_code
